import { Injectable } from '@nestjs/common';
import { ClsService, ClsStore } from 'nestjs-cls';

interface ClsStoreFoo extends ClsStore {
  foo: string;
  barz: AbortController;
  bar?: AbortController;
}

@Injectable()
export class AppService {
  constructor(private readonly cls: ClsService<ClsStoreFoo>) {}

  getHello(): void {


    this.cls.get('foo'); // string
    this.cls.get('bar'); // AbortController | undefined
    this.cls.get('bar.signal'); // AbortSignal
    this.cls.get('bar.abort'); // AbortController['abort']
    this.cls.get('barz.signal'); // AbortSignal


  }
}
